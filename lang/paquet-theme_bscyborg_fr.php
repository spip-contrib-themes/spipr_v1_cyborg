<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// T
	'theme_bscyborg_description' => 'Jet black and electric blue',
	'theme_bscyborg_slogan' => 'Jet black and electric blue',
);
